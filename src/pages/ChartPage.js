import React, { Component } from 'react'
import {
    HorizontalGridLines,
    VerticalGridLines,
    XAxis,
    XYPlot,
    YAxis, LineSeries,
    VerticalBarSeries,
    LabelSeries,
} from 'react-vis';
import axios from 'axios'
import Header from "../components/general/HeaderMain"
import _ from 'lodash';
const data = [
    { x: "Jan", y: 8, },
    { x: "Feb", y: 50 },
    { x: "Mar", y: 4 },
    { x: "Apr", y: 915 },
    { x: "May", y: 100 },
    { x: "Jun", y: 7 },
    { x: "Jul", y: 789 },
    { x: "Aug", y: 30 },
    { x: "Sep", y: 2 },
    { x: "Oct", y: 55 },
    { x: "Nov", y: 100 },
    { x: "Dec", y: 50 }
];
const data2 = [
    { x: "Jan", y: 1000, },
    { x: "Feb", y: 50 },
    { x: "Mar", y: 4 },
    { x: "Apr", y: 915 },
    { x: "May", y: 13 },
    { x: "Jun", y: 7 },
    { x: "Jul", y: 633 },
    { x: "Aug", y: 30 },
    { x: "Sep", y: 299 },
    { x: "Oct", y: 55 },
    { x: "Nov", y: 6 },
    { x: "Dec", y: 500 }
];
export default class ChartPage extends Component {
    state = {
        dataRef: {
            timestamp: 0,
            role: "",
            id: 0
        },
        chartCreate: [
            { x: 0, y: 0 }
        ],
        chartManagementPost: [
            { x: 0, y: 0 }
        ]

    }
    getUsers = () => {
        axios.get("http://localhost:8080/getalluser").then((response) => {
            const data = response.data
            this.setState({ data })
            if (data) {
                const chart = _.map(data, (newValue) => {
                    let date = new Date(newValue.timestamp)
                    let month = date.getMonth()
                    return { x: month, y: newValue.count }
                });
                const sort = _.sortBy(chart, ['x', 'y'])
                this.setState({ chartCreate: sort })
            }
        }).catch((error) => {
            console.log(error)
        })
    }
    getBlogs = () => {
        axios.get("http://localhost:8080/all/blog").then((response) => {
            const data = response.data
            this.setState({ data })
            if (data) {
                const chart = _.map(data, (newValue) => {
                    let date = new Date(newValue.timestamp)
                    let month = date.getMonth()
                    return { x: month, y: newValue.count }
                });
                const sort = _.sortBy(chart, ['x', 'y'])
                this.setState({ chartManagementPost: sort })
            }
        }).catch((error) => {
            console.log(error)
        })
    }
    componentWillMount() {
        this.getUsers()
    }
    render() {
        return (
            <div>
                <div>
                    <Header />
                </div>
                <div style={{ flex: 1, alignItems: 'center', justifyContent: 'left', display: 'flex', paddingLeft: 100, paddingTop: "20px" }}>
                    <h1 style={{ color: "#3498EF", fontSize: 36, borderColor: "#0B49F5", border: 1 }}>Admin dashboard</h1>
                </div>
                <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", margin: 80, borderRadius: 50, boxShadow: "10px 10px 5px grey" }}>
                    <div style={{ flex: 1, alignItems: 'center', justifyContent: 'left', display: 'flex', paddingLeft: 100, paddingTop: "50px", paddingBottom: "20px" }}>
                        <h1 style={{ fontSize: 24, color: "#0635F1" }}>จำนวนคนเข้าใช้งานระบบ</h1>
                    </div>
                    <div style={{ justifyContent: "center", display: "flex", margin: 20 }}>
                        <XYPlot width={1000} height={500} xType="ordinal">
                            <VerticalGridLines />
                            <HorizontalGridLines />
                            <XAxis title="เดือน (1-12)" />
                            <YAxis title="จำนวนสมาชิกที่เข้าสู่ระบบ (รอบ)" />
                            <LineSeries data={data} style={{ stroke: 'violet', strokeWidth: 3 }} />
                        </XYPlot>
                    </div>

                </div>
                <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", margin: 80, borderRadius: 50, boxShadow: "10px 10px 5px grey" }}>
                    <div style={{ flex: 1, alignItems: 'center', justifyContent: 'left', display: 'flex', paddingLeft: 100, paddingTop: "50px", paddingBottom: "20px" }}>
                        <h1 style={{ fontSize: 24, color: "#0635F1" }}>จัดการโพส</h1>
                    </div>
                    <div style={{ justifyContent: "center", display: "flex", margin: 20 }}>
                        <XYPlot width={1000} height={500} xType="ordinal">
                            <VerticalGridLines />
                            <HorizontalGridLines />
                            <XAxis title="เดือน (1-12)" />
                            <YAxis title="จำนวนโพส" />
                            <VerticalBarSeries data={data2} color="#FF7F50" />
                        </XYPlot>
                    </div>
                </div>

            </div>
        )
    }
}
