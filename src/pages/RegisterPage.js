import React, { Component } from "react";
import imageSlide1 from "../assets/images/imageSlide-1.png";
import { Input, Button, message, Form, Radio } from "antd";
import Header from "../components/general/Header";
import { auth } from "../firebase";
import axios from "axios";

class RegisterPage extends Component {
  state = {
    email: "",
    password: "",
    name: "",
    gender: "",
    isLoading: false,
    interested: "",
    aptitude: ""
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onEmailChange = event => {
    const email = event.target.value;
    this.setState({ email });
  };
  onFullNameChange = event => {
    const name = event.target.value;
    this.setState({ name });
  };
  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };
  onInterestedChange = event => {
    const interested = event.target.value;
    this.setState({ interested });
  };
  onAptitudeChange = event => {
    const aptitude = event.target.value;
    this.setState({ aptitude });
  };
  onGenderChange = event => {
    const gender = event.target.value;
    this.setState({ gender });
  };

  onClickRegisterButton = event => {
    this.setState({ isLoading: true });
    const email = this.state.email;
    const password = this.state.password;
    const isEailValid = this.validateEmail(email);
    if (isEailValid) {
      try {
        auth
          .createUserWithEmailAndPassword(email, password)
          .then(() => {
            this.setState({ isLoading: false });
            message.success("Successfully", 1, () => {
              this.props.history.push("/login");
            });
          })
          .catch(err => {
            this.setState({ isLoading: false });
            message.error(err.message, 1);
          });
        axios({
          url: "http://localhost:8080/users/register",
          method: "post",
          data: {
            email: this.state.email,
            password: this.state.password,
            name: this.state.name,
            aptitude: this.state.aptitude,
            gerder: this.state.gender,
            interested: this.state.interested
          }
        })
          .then(response => {})
          .catch(e => {
            console.log("error", e);
          });
      } catch (error) {
        message.error("สมัครสมาชิกไม่สำเร็จ", 1);
      }
    } else {
      this.setState({ isLoading: false });
      message.error("Email invalid!", 1);
    }
  };

  render() {
    return (
      <div>
        <div>
          <Header />
        </div>
        <div
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            height: 900
          }}
        >
          <div
            style={{
              backgroundColor: "#f28c8c",
              height: 700,
              width: 1000,
              borderRadius: 80,
              textAlign: "center"
            }}
          >
            <div style={{ textAlign: "center" }}>
              <h1 style={{ fontSize: 50, margin: 15 }}>Register</h1>
            </div>
            <div
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                flexDirection: "column",
                margin: 10
              }}
            >
              <div
                style={{
                  width: "50%",
                  alignItems: "flex-start",
                  display: "flex"
                }}
              >
                <h3>Email</h3>
              </div>
              <div style={{ width: "50%" }}>
                <Input
                  placeholder="Enter Email"
                  style={{ borderRadius: 5 }}
                  size="large"
                  onChange={this.onEmailChange}
                />
              </div>
            </div>
            <div
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                flexDirection: "column",
                margin: 10
              }}
            >
              <div
                style={{
                  width: "50%",
                  alignItems: "flex-start",
                  display: "flex"
                }}
              >
                <h3>Password</h3>
              </div>
              <div style={{ width: "50%" }}>
                <Input.Password
                  type="password"
                  placeholder="Enter Password"
                  style={{ borderRadius: 5 }}
                  size="large"
                  onChange={this.onPasswordChange}
                />
              </div>
            </div>
            <div
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                flexDirection: "column",
                margin: 10
              }}
            >
              <div
                style={{
                  width: "50%",
                  alignItems: "flex-start",
                  display: "flex"
                }}
              >
                <h3>Fullname</h3>
              </div>
              <div style={{ width: "50%" }}>
                <Input
                  placeholder="Enter fullname"
                  style={{ borderRadius: 5 }}
                  size="large"
                  onChange={this.onFullNameChange}
                />
              </div>
            </div>
            <div
              style={{
                alignItems: "center",
                display: "flex",
                flexDirection: "column",
                margin: 10
              }}
            >
              <div
                style={{
                  width: "50%",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "strech"
                }}
              >
                <div>
                  <h3>Gender</h3>
                </div>
                <div style={{ marginRight: 250, marginTop: 3 }}>
                  <Radio.Group
                    onChange={this.onGenderChange}
                    value={this.state.gender}
                  >
                    <Radio value={"male"}>male</Radio>
                    <Radio value={"female"}>femele</Radio>
                  </Radio.Group>
                </div>
              </div>
            </div>
            <div
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                flexDirection: "column",
                margin: 10
              }}
            >
              <div
                style={{
                  width: "50%",
                  alignItems: "flex-start",
                  display: "flex"
                }}
              >
                <h3>Interest</h3>
              </div>
              <div style={{ width: "50%" }}>
                <Input
                  placeholder="Enter interest"
                  style={{ borderRadius: 5 }}
                  size="large"
                  onChange={this.onInterestedChange}
                />
              </div>
            </div>
            <div
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                flexDirection: "column",
                margin: 10
              }}
            >
              <div
                style={{
                  width: "50%",
                  alignItems: "flex-start",
                  display: "flex"
                }}
              >
                <h3>Aptitude</h3>
              </div>
              <div style={{ width: "50%" }}>
                <Input
                  placeholder="Enter fullname"
                  style={{ borderRadius: 5 }}
                  size="large"
                  onChange={this.onAptitudeChange}
                />
              </div>
            </div>
            <Button
              size="large"
              style={{
                backgroundColor: "#2FEB3C",
                borderRadius: 50,
                height: 44,
                width: 120,
                marginTop: 30
              }}
              onClick={() => this.onClickRegisterButton()}
              loading={this.state.isLoading}
            >
              <label style={{ color: "white" }}>Register</label>
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
export default RegisterPage;
