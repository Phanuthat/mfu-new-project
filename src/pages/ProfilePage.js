import React, { Component } from 'react'
import imageSlide1 from '../assets/images/imageSlide-1.png'
import {
  Avatar,
  Button,
  List,
  Card,
  Layout,
  Icon,
  Modal,
  message,
  Input,
  Radio,
  Dropdown,
  Menu
} from 'antd'
import HeaderMain from '../components/general/HeaderMain'
import Header from '../components/general/Header'
import { Element, animateScroll as scroll } from 'react-scroll'
import HorizontalScroll from 'react-scroll-horizontal'
import axios from 'axios'
const { Meta } = Card

class ProfilePage extends Component {
  state = {
    imageUrl: '',
    email: 'Athena Nicasio ',
    user: [],
    informationUser: [],
    isLoggedIn: false,
    blogs: [],
    isClickModal: false,
    userEdit: [],
    typeBlog: [],
    typeName: 'สิ่งที่สนใจ'
  }
  menu = () => {
    return (
      <Menu>
        {this.state.typeBlog.map(item => (
          <Menu.Item
            onClick={() => {
              this.setState({ typeName: item.typeName, typeId: item.id })
              this.onChangeUserInterested()
            }}
          >
            {item.typeName}
          </Menu.Item>
        ))}
      </Menu>
    )
  }
  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data')
    const user = jsonStr && JSON.parse(jsonStr)
    this.setState({ user: user, userEdit: user })
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn
    if (isLoggedIn) {
      this.setState({ isLoggedIn: isLoggedIn })
      if (user.imageUrl) {
        this.setState({ imageUrl: user.imageUrl })
      } else {
        this.setState({
          imageUrl: 'https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png'
        })
      }
    }
    axios
      .get(`http://localhost:8080/blog/user/${user.id}`)
      .then(res => {
        this.setState({ blogs: res.data })
      })
      .catch(error => {
        console.log(error)
      })
    axios
      .get(`http://localhost:8080/user/info/${user.id}`)
      .then(res => {
        this.setState({ informationUser: res.data })
      })
      .catch(error => {
        console.log(error)
      })
    axios({
      url: 'http://localhost:8080/blogtype',
      method: 'get'
    }).then(res => {
      this.setState({ typeBlog: res.data })
    })
  }

  getBlogByUserId = () => {
    axios
      .get(`http://localhost:8080/blog/user/${this.state.user.id}`)
      .then(res => {
        this.setState({ blogs: res.data })
      })
      .catch(error => {
        console.log(error)
      })
  }

  editInformationUserInLocalStorage = () => {
    localStorage.setItem(
      'user-data',
      JSON.stringify({
        email: this.state.userEdit.email,
        isLoggedIn: true,
        name: this.state.userEdit.name,
        imageUrl: this.state.userEdit.image,
        phone: this.state.userEdit.phone,
        interested: this.state.userEdit.interested,
        aptitude: this.state.userEdit.aptitude,
        role: this.state.userEdit.role,
        date: this.state.userEdit.date,
        count: this.state.userEdit.count,
        id: this.state.userEdit.id,
        gender: this.state.userEdit.gender,
        facebook: this.state.userEdit.facebook,
        line: this.state.userEdit.line
      })
    )
    axios({
      url: `http://localhost:8080/user/edit/${this.state.user.id}`,
      method: 'put',
      data: {
        name: this.state.userEdit.name,
        phone: this.state.userEdit.phone,
        interested: this.state.userEdit.interested,
        gerder: this.state.userEdit.gender,
        aptitude: this.state.userEdit.aptitude,
        facebook: this.state.userEdit.facebook,
        line: this.state.userEdit.line
      }
    })
      .then(res => {
        message.success('แก้ไข Profile สำเร็จ', 1)
      })
      .catch(e => {
        message.error('แก้ไข Profile ไม่สำเร็จ', 1)
      })
    this.setState({ user: this.state.userEdit })
  }

  onClickUnFriendIcon = item => {
    axios({
      url: `http://localhost:8080/unfriend?userId=${this.state.user.id}&friendId=${item.id}`,
      method: 'delete'
    })
      .then(res => {
        axios
          .get(`http://localhost:8080/user/info/${this.state.user.id}`)
          .then(res => {
            this.setState({ informationUser: res.data })
          })
          .catch(error => {
            console.log(error)
          })
        message.success('ลบเพื่อนสำเร็จ', 1)
      })
      .catch(e => {
        axios
          .get(`http://localhost:8080/user/info/${this.state.user.id}`)
          .then(res => {
            this.setState({ informationUser: res.data })
          })
          .catch(error => {
            console.log(error)
          })
        message.error('ลบเพื่อนไม่สำเร็จ', 1)
      })
  }

  onClickUnFollowingIcon = item => {
    axios({
      url: `http://localhost:8080/unfollower?userId=${this.state.user.id}&followerId=${item.id}`,
      method: 'delete'
    })
      .then(res => {
        axios
          .get(`http://localhost:8080/user/info/${this.state.user.id}`)
          .then(res => {
            this.setState({ informationUser: res.data })
          })
          .catch(error => {
            console.log(error)
          })
        message.success('ยกเลิกการติดตามสำเร็จ', 1)
      })
      .catch(e => {
        axios
          .get(`http://localhost:8080/user/info/${this.state.user.id}`)
          .then(res => {
            this.setState({ informationUser: res.data })
          })
          .catch(error => {
            console.log(error)
          })
        message.error('ยกเลิกการติดตามไม่สำเร็จ', 1)
      })
  }

  onClickEditButton = blogId => {
    this.props.history.push('/editblog', { id: blogId })
  }

  OnModalCancel = () => {
    this.setState({ isClickModal: false, userEdit: this.state.user })
  }

  OnModalOk = () => {
    this.setState({ isClickModal: false })
    this.editInformationUserInLocalStorage()
  }

  onClickEditProfileButton = () => {
    this.setState({ isClickModal: true })
  }

  onChangeUserName = event => {
    const value = event.target.value
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        name: value
      }
    }))
  }
  onChangeUserGender = event => {
    const value = event.target.value
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        gender: value
      }
    }))
  }
  onChangeUserAptitude = event => {
    const value = event.target.value
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        aptitude: value
      }
    }))
  }
  onChangeUserInterested = () => {
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        interested: this.state.typeName
      }
    }))
  }
  onChangeUserPhone = event => {
    const value = event.target.value
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        phone: value
      }
    }))
  }
  onChangeUserFacebook = event => {
    const value = event.target.value
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        facebook: value
      }
    }))
  }
  onChangeUserLine = event => {
    const value = event.target.value
    this.setState(prevState => ({
      userEdit: {
        ...prevState.userEdit,
        line: value
      }
    }))
  }

  checkItemFavorited = item => {
    const type = item.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        return ''
      } else {
        return 'danger'
      }
    })
    if (item.favorites <= 0) {
      return 'danger'
    }
    return type
  }

  changeBackgroundColor = item => {
    const backgroundColor = item.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        return 'red'
      } else {
        return ''
      }
    })
    return backgroundColor
  }

  changeColor = item => {
    const color = item.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        return 'white'
      } else {
        return ''
      }
    })
    return color
  }

  onClickFavorite = blogClick => {
    blogClick.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        axios({
          url: `http://localhost:8080/unfavorite/${blogClick.id}/${item.id}`,
          method: 'delete'
        }).then(res => {
          this.getBlogByUserId()
          message.success('เลิกถูกใจสำเร็จ', 1)
        })
      } else {
        axios({
          url: `http://localhost:8080/add/favorite/${blogClick.id}/${this.state.user.id}`,
          method: 'post'
        }).then(res => {
          this.getBlogByUserId()
          message.success('ถูกใจสำเร็จ', 1)
        })
      }
    })
    if (blogClick.favorites <= 0) {
      axios({
        url: `http://localhost:8080/add/favorite/${blogClick.id}/${this.state.user.id}`,
        method: 'post'
      }).then(res => {
        this.getBlogByUserId()
        message.success('ถูกใจสำเร็จ', 1)
      })
    }
  }

  onClickCreatePostButton = () => {
    this.props.history.push('/addblog')
  }

  render() {
    if (
      this.state.informationUser.friends &&
      this.state.user.role === 'member'
    ) {
      return (
        <div>
          {this.state.isLoggedIn === true ? (
            <HeaderMain isLoggedIn={this.state.isLoggedIn} />
          ) : (
            <Header />
          )}
          <div style={{ flexDirection: 'row', display: 'flex' }}>
            <div
              style={{
                width: '35%',
                height: 800,
                textAlign: 'center',
                paddingTop: 50,
                backgroundColor: '#f2f2f2',
                margin: 20,
                borderRadius: 30
              }}
            >
              <h1>Profile</h1>
              <div>
                <Avatar size={120} src={this.state.imageUrl} />
              </div>
              <div></div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  ชื่อผู้ใช้ :{' '}
                </label>
                <label style={{ fontSize: 20 }}>{this.state.user.name}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>เพศ : </label>
                <label style={{ fontSize: 20 }}>{this.state.user.gender}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  อีเมลล์ :{' '}
                </label>
                <label style={{ fontSize: 20 }}>{this.state.user.email}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  ความถนัด :{' '}
                </label>
                <label style={{ fontSize: 20 }}>
                  {this.state.user.aptitude}
                </label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  สิ่งที่สนใจ :{' '}
                </label>
                <label style={{ fontSize: 20 }}>
                  {this.state.user.interested}
                </label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  เบอร์โทร :{' '}
                </label>
                <label style={{ fontSize: 20 }}>{this.state.user.phone}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  Facebook :{' '}
                </label>
                <label style={{ fontSize: 20 }}>
                  {this.state.user.facebook}
                </label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>Line : </label>
                <label style={{ fontSize: 20 }}>{this.state.user.line}</label>
              </div>
              <div>
                <Button
                  type="primary"
                  size="large"
                  style={{
                    margin: 20,
                    backgroundColor: '#2BBC65',
                    borderColor: '#2BBC65',
                    borderRadius: 50,
                    height: 44,
                    width: 90
                  }}
                  onClick={() => this.onClickEditProfileButton()}
                >
                  แก้ไข
                </Button>
              </div>
            </div>
            <div
              style={{
                width: '65%',
                justifyContent: 'center',
                backgroundColor: '#cccccc',
                margin: 20,
                borderRadius: 30
              }}
            >
              <Element
                style={{
                  position: 'relative',
                  overflow: 'scroll',
                  marginBottom: '100px',
                  margin: '50px',
                  backgroundColor: '#f2f2f2',
                  borderRadius: 30
                }}
              >
                <div
                  style={{
                    margin: 10,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <div style={{ marginLeft: 20 }}>
                    <h1>เพื่อน</h1>
                  </div>
                  <Layout style={{ flexDirection: 'row' }}>
                    {this.state.informationUser.friends.map(item => (
                      <div>
                        <Card
                          hoverable
                          style={{ width: 150, height: 200, margin: 10 }}
                          cover={
                            <div
                              style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                display: 'flex'
                              }}
                            >
                              <div
                                style={{
                                  position: 'absolute',
                                  width: '100%',
                                  height: 140,
                                  justifyContent: 'flex-end',
                                  display: 'flex'
                                }}
                              >
                                <Icon
                                  type="close-circle"
                                  style={{ fontSize: 20 }}
                                  onClick={() => this.onClickUnFriendIcon(item)}
                                />
                              </div>
                              {item.image === null ? (
                                <img
                                  alt="example"
                                  src="https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png"
                                  height={140}
                                  width={140}
                                  style={{ borderRadius: 100 }}
                                />
                              ) : (
                                <img
                                  alt="example"
                                  src={item.image}
                                  height={140}
                                  width={140}
                                  style={{ borderRadius: 100 }}
                                />
                              )}
                            </div>
                          }
                        >
                          <Meta title={item.name} />
                        </Card>
                      </div>
                    ))}
                  </Layout>
                </div>
              </Element>
              <Element
                style={{
                  position: 'relative',
                  overflow: 'scroll',
                  marginBottom: '100px',
                  margin: '50px',
                  backgroundColor: '#f2f2f2',
                  borderRadius: 30
                }}
              >
                <div
                  style={{
                    margin: 10,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <div style={{ marginLeft: 20 }}>
                    <h1>กำลังติดตาม</h1>
                  </div>
                  <Layout style={{ flexDirection: 'row' }}>
                    {this.state.informationUser.following.map(item => (
                      <div>
                        <Card
                          hoverable
                          style={{ width: 150, height: 200, margin: 10 }}
                          cover={
                            <div
                              style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                display: 'flex'
                              }}
                            >
                              <div
                                style={{
                                  position: 'absolute',
                                  width: '100%',
                                  height: 140,
                                  justifyContent: 'flex-end',
                                  display: 'flex'
                                }}
                              >
                                <Icon
                                  type="close-circle"
                                  style={{ fontSize: 20 }}
                                  onClick={() =>
                                    this.onClickUnFollowingIcon(item)
                                  }
                                />
                              </div>
                              {item.image === null ? (
                                <img
                                  alt="example"
                                  src="https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png"
                                  height={140}
                                  width={140}
                                  style={{ borderRadius: 100 }}
                                />
                              ) : (
                                <img
                                  alt="example"
                                  src={item.image}
                                  height={140}
                                  width={140}
                                  style={{ borderRadius: 100 }}
                                />
                              )}
                            </div>
                          }
                        >
                          <Meta title={item.name} />
                        </Card>
                      </div>
                    ))}
                  </Layout>
                </div>
              </Element>
              <Element
                style={{
                  position: 'relative',
                  overflow: 'scroll',
                  marginBottom: '100px',
                  margin: '50px',
                  backgroundColor: '#f2f2f2',
                  borderRadius: 30
                }}
              >
                <div
                  style={{
                    margin: 10,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <div style={{ marginLeft: 20 }}>
                    <h1>เพื่อนที่ติดตาม</h1>
                  </div>
                  <Layout style={{ flexDirection: 'row' }}>
                    {this.state.informationUser.followers.map(item => (
                      <div>
                        <Card
                          hoverable
                          style={{ width: 150, height: 200, margin: 10 }}
                          cover={
                            <div
                              style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                display: 'flex'
                              }}
                            >
                              {item.image === null ? (
                                <img
                                  alt="example"
                                  src="https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png"
                                  height={140}
                                  width={140}
                                  style={{ borderRadius: 100 }}
                                />
                              ) : (
                                <img
                                  alt="example"
                                  src={item.image}
                                  height={140}
                                  width={140}
                                  style={{ borderRadius: 100 }}
                                />
                              )}
                            </div>
                          }
                        >
                          <Meta title={item.name} />
                        </Card>
                      </div>
                    ))}
                  </Layout>
                </div>
              </Element>
              <div
                style={{
                  borderStyle: 'solid',
                  borderWidth: '1px',
                  borderColor: 'black',
                  margin: 20,
                  borderRadius: 50,
                  backgroundColor: '#f2f2f2'
                }}
              >
                <div
                  style={{
                    marginLeft: 150,
                    marginRight: 150,
                    marginTop: 50,
                    flexDirection: 'row',
                    display: 'flex'
                  }}
                >
                  <div
                    style={{
                      alignItems: 'center',
                      display: 'flex',
                      width: '60%'
                    }}
                  >
                    <h2 style={{ fontSize: 36 }}>เรื่องที่เราโพส</h2>
                  </div>
                  <div
                    style={{
                      width: '100%',
                      justifyContent: 'flex-start',
                      display: 'flex'
                    }}
                  >
                    {this.state.user.role === 'member' ? (
                      <Button
                        type="primary"
                        size="large"
                        style={{
                          margin: 20,
                          backgroundColor: '#00cc66',
                          borderColor: '#00cc66',
                          borderRadius: 200
                        }}
                        onClick={() => this.onClickCreatePostButton()}
                      >
                        สร้างโพส
                      </Button>
                    ) : null}

                    <Button
                      type="primary"
                      size="large"
                      style={{
                        margin: 20,
                        backgroundColor: '#2E65E5',
                        borderColor: '#2E65E5',
                        borderRadius: 200
                      }}
                    >
                      จัดการโพส
                    </Button>
                  </div>
                </div>
                <Element
                  style={{
                    position: 'relative',
                    height: '800px',
                    overflow: 'scroll',
                    marginBottom: '100px'
                  }}
                >
                  <List
                    itemLayout="horizontal"
                    dataSource={this.state.blogs}
                    renderItem={item => (
                      <div
                        style={{
                          paddingTop: '50px',
                          marginLeft: 100,
                          marginRight: 100,
                          justifyContent: 'left',
                          display: 'flex'
                        }}
                      >
                        <Card
                          style={{
                            width: '100%',
                            borderStyle: 'solid',
                            borderWidth: '1px',
                            borderColor: 'black',
                            borderRadius: 50,
                            marginBottom: 50
                          }}
                        >
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <Avatar size={120} src={this.state.imageUrl} />
                              }
                              title={
                                <a href="https://ant.design">
                                  ชื่อ ผู้ใช้ : {item.userData.name}
                                </a>
                              }
                              description={
                                <div
                                  style={{
                                    flexDirection: 'row',
                                    display: 'flex'
                                  }}
                                >
                                  <div>
                                    <label>เรื่องที่โพส : {item.title} </label>
                                    <p>ประเภท : {item.blogType.typeName} </p>
                                  </div>
                                  <div
                                    style={{
                                      width: '100%',
                                      justifyContent: 'flex-end',
                                      display: 'flex'
                                    }}
                                  >
                                    <div
                                      style={{
                                        flexDirection: 'column',
                                        display: 'flex',
                                        alignItems: 'center',
                                        marginTop: 10,
                                        marginRight: 30
                                      }}
                                    >
                                      <Button
                                        key="submit"
                                        icon="heart"
                                        type={this.checkItemFavorited(item)}
                                        size="large"
                                        shape="circle"
                                        style={{
                                          backgroundColor: this.changeBackgroundColor(
                                            item
                                          ),
                                          color: this.changeColor(item)
                                        }}
                                        onClick={() =>
                                          this.onClickFavorite(item)
                                        }
                                      />
                                      <h3>{item.amountOfFavorites} คน</h3>
                                    </div>
                                    <Icon
                                      onClick={() =>
                                        this.onClickEditButton(item.id)
                                      }
                                      type="edit"
                                      style={{
                                        fontSize: 38,
                                        color: '#00cc66',
                                        marginTop: 10
                                      }}
                                    />
                                  </div>
                                </div>
                              }
                            />
                          </List.Item>
                        </Card>
                      </div>
                    )}
                  />
                </Element>
              </div>
            </div>
          </div>
          <div>
            <Modal
              title="แก้ไข Profile"
              visible={this.state.isClickModal}
              onOk={() => {
                this.OnModalOk()
              }}
              onCancel={() => {
                this.OnModalCancel()
              }}
            >
              <Input
                placeholder="Enter your name"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.name}
                onChange={this.onChangeUserName}
                title="ชื่อผู้ใช้"
                addonBefore="ชื่อผู้ใช้"
              />
              <Radio.Group
                onChange={this.onChangeUserGender}
                value={this.state.userEdit.gender}
              >
                <Radio value="ชาย">ชาย</Radio>
                <Radio value="หญิง">หญิง</Radio>
              </Radio.Group>
              <Input
                placeholder="Enter your aptitude"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.aptitude}
                onChange={this.onChangeUserAptitude}
                title="ความถนัด"
                addonBefore="ความถนัด"
              />
              <Dropdown overlay={this.menu} trigger={['click']} size="500">
                <Button>
                  {this.state.typeName} <Icon type="down" />
                </Button>
              </Dropdown>
              <Input
                placeholder="Enter your interested"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.interested}
                onChange={this.onChangeUserInterested}
                title="สิ่งที่สนใจ"
                addonBefore="สิ่งที่สนใจ"
              />
              <Input
                placeholder="Enter your phone"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.phone}
                onChange={this.onChangeUserPhone}
                title="เบอร์โทร"
                addonBefore="เบอร์โทร"
              />
              <Input
                placeholder="Enter your facebook"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.facebook}
                onChange={this.onChangeUserFacebook}
                title="Facebook"
                addonBefore="Facebook"
              />
              <Input
                placeholder="Enter your line"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.line}
                onChange={this.onChangeUserLine}
                title="Line"
                addonBefore="Line"
              />
            </Modal>
          </div>
        </div>
      )
    } else {
      return (
        <div>
          {this.state.isLoggedIn === true ? (
            <HeaderMain isLoggedIn={this.state.isLoggedIn} />
          ) : (
            <Header />
          )}
          <div style={{ justifyContent: 'center', display: 'flex' }}>
            <div
              style={{
                width: '35%',
                height: 800,
                textAlign: 'center',
                paddingTop: 50,
                backgroundColor: '#f2f2f2',
                margin: 20,
                borderRadius: 30
              }}
            >
              <h1>Profile</h1>
              <div>
                <Avatar size={120} src={this.state.imageUrl} />
              </div>
              <div></div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  ชื่อผู้ใช้ :{' '}
                </label>
                <label style={{ fontSize: 20 }}>{this.state.user.name}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>เพศ : </label>
                <label style={{ fontSize: 20 }}>{this.state.user.gender}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  อีเมลล์ :{' '}
                </label>
                <label style={{ fontSize: 20 }}>{this.state.user.email}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  ความถนัด :{' '}
                </label>
                <label style={{ fontSize: 20 }}>
                  {this.state.user.aptitude}
                </label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  สิ่งที่สนใจ :{' '}
                </label>
                <label style={{ fontSize: 20 }}>
                  {this.state.user.interested}
                </label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  เบอร์โทร :{' '}
                </label>
                <label style={{ fontSize: 20 }}>{this.state.user.phone}</label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>
                  Facebook :{' '}
                </label>
                <label style={{ fontSize: 20 }}>
                  {this.state.user.facebook}
                </label>
              </div>
              <div style={{ flexDirection: 'row', margin: 10 }}>
                <label style={{ fontSize: 22, color: 'black' }}>Line : </label>
                <label style={{ fontSize: 20 }}>{this.state.user.line}</label>
              </div>
              <div>
                <Button
                  type="primary"
                  size="large"
                  style={{
                    margin: 20,
                    backgroundColor: '#2BBC65',
                    borderColor: '#2BBC65',
                    borderRadius: 50,
                    height: 44,
                    width: 90
                  }}
                  onClick={() => this.onClickEditProfileButton()}
                >
                  แก้ไข
                </Button>
              </div>
            </div>
          </div>
          <div>
            <Modal
              title="แก้ไข Profile"
              visible={this.state.isClickModal}
              onOk={() => {
                this.OnModalOk()
              }}
              onCancel={() => {
                this.OnModalCancel()
              }}
            >
              <Input
                placeholder="Enter your name"
                style={{ borderRadius: 5 }}
                size="large"
                value={this.state.userEdit.name}
                onChange={this.onChangeUserName}
                title="ชื่อผู้ใช้"
                addonBefore="ชื่อผู้ใช้"
              />
              <div style={{ margin: 5 }}>
                <Radio.Group
                  onChange={this.onChangeUserGender}
                  value={this.state.userEdit.gender}
                >
                  <Radio value="ชาย">ชาย</Radio>
                  <Radio value="หญิง">หญิง</Radio>
                </Radio.Group>
              </div>
              <div style={{ margin: 5 }}>
                <Input
                  placeholder="Enter your aptitude"
                  style={{ borderRadius: 5 }}
                  size="large"
                  value={this.state.userEdit.aptitude}
                  onChange={this.onChangeUserAptitude}
                  title="ความถนัด"
                  addonBefore="ความถนัด"
                />
              </div>
              <div style={{ margin: 5 }}>
                <Dropdown overlay={this.menu} trigger={['click']} size="large">
                  <Button style={{ width: '100%' }}>
                    {this.state.typeName} <Icon type="down" />
                  </Button>
                </Dropdown>
              </div>
              <div style={{ margin: 5 }}>
                <Input
                  placeholder="Enter your phone"
                  style={{ borderRadius: 5 }}
                  size="large"
                  value={this.state.userEdit.phone}
                  onChange={this.onChangeUserPhone}
                  title="เบอร์โทร"
                  addonBefore="เบอร์โทร"
                />
              </div>
              <div style={{ margin: 5 }}>
                <Input
                  placeholder="Enter your facebook"
                  style={{ borderRadius: 5 }}
                  size="large"
                  value={this.state.userEdit.facebook}
                  onChange={this.onChangeUserFacebook}
                  title="Facebook"
                  addonBefore="Facebook"
                />
              </div>
              <div style={{ margin: 5 }}>
                <Input
                  placeholder="Enter your line"
                  style={{ borderRadius: 5 }}
                  size="large"
                  value={this.state.userEdit.line}
                  onChange={this.onChangeUserLine}
                  title="Line"
                  addonBefore="Line"
                />
              </div>
            </Modal>
          </div>
        </div>
      )
    }
  }
}

export default ProfilePage
