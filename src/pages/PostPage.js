import React, { Component } from 'react'
import {
  List,
  Card,
  Typography,
  Avatar,
  Button,
  Input,
  message,
  Dropdown,
  Icon,
  Menu
} from 'antd'

import HeaderMain from '../components/general/HeaderMain'
import Header from '../components/general/Header'
import { Element, animateScroll as scroll } from 'react-scroll'
import axios from 'axios'
const { Search } = Input
const { Title } = Typography

class ManagePostPage extends Component {
  state = {
    imageUrl: 'https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png',
    email: 'Athena Nicasio ',
    isLoggedIn: false,
    blogs: [],
    user: [],
    search: '',
    typeBlog: [],
    typeName: 'ประภท'
  }

  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data')
    const user = jsonStr && JSON.parse(jsonStr)
    this.setState({ user: user })
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn
    if (isLoggedIn) {
      this.setState({ isLoggedIn: isLoggedIn })
    }
    this.getAllBlog()
    axios({
      url: 'http://localhost:8080/blogtype',
      method: 'get'
    }).then(res => {
      this.setState({ typeBlog: res.data })
    })
  }
  componentWillMount() {
    this.getAllBlog()
  }
  onSearch = event => {
    this.setState({ search: event })
  }
  getAllBlog = () => {
    axios
      .get('http://localhost:8080/all/blog')
      .then(res => {
        if (res.data) {
          let data = res.data
          this.setState({ blogs: data })
        } else {
          console.log('data not found')
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  onClickCreatePost = () => {
    this.props.history.push('/addblog')
  }

  onClickPost = blogId => {
    this.props.history.push('/blog', { id: blogId })
  }

  checkItemFavorited = item => {
    const type = item.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        return ''
      } else {
        return 'danger'
      }
    })
    if (item.favorites <= 0) {
      return 'danger'
    }
    return type
  }

  changeBackgroundColor = item => {
    const backgroundColor = item.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        return 'red'
      } else {
        return ''
      }
    })
    return backgroundColor
  }

  changeColor = item => {
    const color = item.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        return 'white'
      } else {
        return ''
      }
    })
    return color
  }

  onClickFavorite = blogClick => {
    blogClick.favorites.map(item => {
      if (item.userData.id === this.state.user.id) {
        axios({
          url: `http://localhost:8080/unfavorite/${blogClick.id}/${item.id}`,
          method: 'delete'
        }).then(res => {
          this.getAllBlog()
          message.success('เลิกถูกใจสำเร็จ', 1)
        })
      } else {
        axios({
          url: `http://localhost:8080/add/favorite/${blogClick.id}/${this.state.user.id}`,
          method: 'post'
        }).then(res => {
          this.getAllBlog()
          message.success('ถูกใจสำเร็จ', 1)
        })
      }
    })
    if (blogClick.favorites <= 0) {
      axios({
        url: `http://localhost:8080/add/favorite/${blogClick.id}/${this.state.user.id}`,
        method: 'post'
      }).then(res => {
        this.getAllBlog()
        message.success('ถูกใจสำเร็จ', 1)
      })
    }
  }
  menu = () => {
    return (
      <Menu>
        {this.state.typeBlog.map(item => (
          <Menu.Item
            onClick={() => {
              this.setState({ typeName: item.typeName, typeId: item.id })
            }}
          >
            {item.typeName}
          </Menu.Item>
        ))}
      </Menu>
    )
  }

  render() {
    let blogList = this.state.blogs
    blogList = blogList.filter(newList => {
      const searchByname =
        newList.userData.name
          .toLowerCase()
          .search(this.state.search.toLowerCase()) !== -1
      const searchByTitle =
        newList.title.toLowerCase().search(this.state.search.toLowerCase()) !==
        -1
      const searchByType =
        newList.blogType.typeName
          .toLowerCase()
          .search(this.state.search.toLowerCase()) !== -1
      return searchByname || searchByTitle || searchByType
    })
    return (
      <div>
        {this.state.isLoggedIn === true ? <HeaderMain /> : <Header />}
        <div
          style={{
            marginTop: 50,
            marginBottom: 50,
            marginLeft: 80,
            marginRight: 80,
            flexDirection: 'row'
          }}
        >
          <div
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              display: 'flex',
              paddingLeft: 100
            }}
          >
            <Title>Post</Title>
            <div
              style={{
                width: '100%',
                justifyContent: 'flex-end',
                display: 'flex',
                paddingRight: 100
              }}
            >
              {' '}
              {this.state.user.role === 'member' ? (
                <Button
                  type="primary"
                  size="large"
                  style={{
                    margin: 20,
                    backgroundColor: '#00cc66',
                    borderColor: '#00cc66',
                    borderRadius: 200
                  }}
                  onClick={() => this.onClickCreatePost()}
                >
                  สร้างโพส
                </Button>
              ) : null}
            </div>
          </div>
          <div
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              display: 'flex',
              paddingLeft: 100
            }}
          >
            <Search
              placeholder="ค้นหาชื่อเรื่องหรือผู้ใช่"
              onSearch={value => this.onSearch(value)}
              style={{ height: 30, width: 500 }}
            />
          </div>
          <div
            style={{
              borderStyle: 'solid',
              borderWidth: '1px',
              borderColor: 'black',
              marginLeft: 80,
              marginRight: 80,
              marginBottom: 80,
              marginTop: 35,
              borderRadius: 50
            }}
          >
            <Element
              style={{
                position: 'relative',
                height: '800px',
                overflow: 'scroll',
                marginBottom: '100px',
                margin: '50px'
              }}
            >
              <div
                style={{
                  justifyContent: 'flex-end',
                  display: 'flex',
                  paddingRight: 100
                }}
              >
                <Dropdown overlay={this.menu} trigger={['click']}>
                  <Button style={{ width: 125 }}>
                    {this.state.typeName} <Icon type="down" />
                  </Button>
                </Dropdown>
              </div>

              <List
                itemLayout="horizontal"
                dataSource={blogList}
                renderItem={item => (
                  <div
                    style={{
                      paddingTop: '50px',
                      marginLeft: 100,
                      marginRight: 100,
                      justifyContent: 'left',
                      display: 'flex'
                    }}
                  >
                    <Card
                      style={{
                        width: '100%',
                        borderStyle: 'solid',
                        borderWidth: '1px',
                        borderColor: 'black',
                        borderRadius: 50,
                        marginBottom: 50
                      }}
                    >
                      <List.Item>
                        <List.Item.Meta
                          avatar={
                            item.userData.imageUrl ? (
                              <img
                                alt="user"
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZt2UXH23Cc225oD9MdnyN8woLt9YTkwfDZoYVsun_xugbCL_rmQ"
                                height={140}
                                width={140}
                                style={{ borderRadius: 100 }}
                              />
                            ) : (
                              <img
                                alt="user"
                                src={
                                  'https://cdn0.iconfinder.com/data/icons/user-pictures/100/matureman1-512.png'
                                }
                                height={140}
                                width={140}
                                style={{ borderRadius: 100 }}
                              />
                            )
                          }
                          title={
                            <h2 style={{ marginTop: 15 }}>
                              ชื่อ ผู้ใช้ : {item.userData.name}
                            </h2>
                          }
                          description={
                            <div
                              style={{ flexDirection: 'row', display: 'flex' }}
                            >
                              <div
                                style={{
                                  flexDirection: 'row',
                                  display: 'flex'
                                }}
                              >
                                <div style={{ width: 300 }}>
                                  <label>เรื่องที่โพส : {item.title} </label>
                                  <p>ประเภท : {item.blogType.typeName} </p>
                                </div>
                              </div>
                              <div
                                style={{
                                  width: '100%',
                                  justifyContent: 'flex-end',
                                  display: 'flex'
                                }}
                              >
                                <div
                                  style={{
                                    flexDirection: 'column',
                                    display: 'flex',
                                    alignItems: 'center',
                                    marginRight: 30
                                  }}
                                >
                                  <Button
                                    key="submit"
                                    icon="heart"
                                    type={this.checkItemFavorited(item)}
                                    size="large"
                                    shape="circle"
                                    style={{
                                      backgroundColor: this.changeBackgroundColor(
                                        item
                                      ),
                                      color: this.changeColor(item)
                                    }}
                                    onClick={() => this.onClickFavorite(item)}
                                  />
                                  <h3>{item.amountOfFavorites} คน</h3>
                                </div>
                                <div>
                                  <Button
                                    type="primary"
                                    size="large"
                                    style={{
                                      backgroundColor: '#2FEB3C',
                                      borderColor: '#2FEB3C',
                                      borderRadius: 50,
                                      height: 44,
                                      width: 90,
                                      marginLeft: 10,
                                      marginRight: 20
                                    }}
                                    onClick={() => {
                                      this.onClickPost(item.id)
                                    }}
                                  >
                                    เปิด
                                  </Button>
                                </div>
                              </div>
                            </div>
                          }
                        />
                      </List.Item>
                    </Card>
                  </div>
                )}
              />
            </Element>
          </div>
        </div>
      </div>
    )
  }
}

export default ManagePostPage
