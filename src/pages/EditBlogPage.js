import React, { Component } from 'react'
import { Button, Dropdown, Avatar, Typography, Row, Col, Input, Icon, Menu, message } from 'antd'
import HeaderMain from '../components/general/HeaderMain'
import CKEditor from 'ckeditor4-react';
import axios from 'axios';

const { Title } = Typography;
const { TextArea } = Input;
export default class EditBlogPage extends Component {

    state = {
        user: [],
        imageUrl: '',
        title: '',
        detail: '',
        facebook: '',
        line: '',
        phone: '',
        typeBlog: [],
        typeName: 'ประเภท',
        typeId: '',
        blog: []
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const user = jsonStr && JSON.parse(jsonStr)
        this.setState({ user: user })
        if (!jsonStr.imageUrl) {
            this.setState({ imageUrl: 'https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png' })
        } else {
            this.setState({ imageUrl: jsonStr.imageUrl })
        }
        axios({
            url: `http://localhost:8080/blog/${this.props.location.state.id}`,
            method: 'get'
        }).then(res => {
            this.setState({ blog: res.data, typeName: res.data.blogType.typeName, title: res.data.title, detail: res.data.detail });
        })
        axios({
            url: 'http://localhost:8080/blogtype',
            method: 'get'
        }).then(res => {
            this.setState({ typeBlog: res.data })
        })
    }

    menu = () => {
        return (
            <Menu>
                {
                    this.state.typeBlog.map(item => (
                        <Menu.Item onClick={() => this.setState({ typeName: item.typeName, typeId: item.id })}>{item.typeName}</Menu.Item>
                    ))
                }
            </Menu>
        )
    }

    onClickEditPost = () => {
        axios({
            url: `http://localhost:8080/blog/edit/${this.props.location.state.id}`,
            method: 'put',
            data: {
                title: this.state.title,
                detail: this.state.detail
            }
        }).then(res => {
            message.success('แก้ไขโพสสำเร็จ', 1);
            setTimeout(() => {
                this.props.history.push('/profile');
            }, 1500);
        }).catch(e => {
            message.error('แก้ไขโพสไม่สำเร็จ', 1);
        })
    }

    onTitleChange = event => {
        const title = event.target.value;
        this.setState({ title });
    };

    onDetailChange = event => {
        const detail = event.target.value;
        this.setState({ detail });
    };

    render() {
        return (
            <div>
                <div>
                    <HeaderMain />
                </div>
                <div style={{ alignItems: 'center', flexDirection: 'column', display: 'flex' }}>
                    <div style={{ margin: 40, width: '80%' }}>
                        <div style={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                            <Title level={2} style={{ textAlign: "center" }}>เรื่อง: </Title>
                            <Input
                                style={{ width: 200, marginTop: 5, marginLeft: 14 }}
                                placeholder="Enter Title of post"
                                onChange={this.onTitleChange}
                                value={this.state.title}
                            />
                            <div style={{ marginTop: 5, marginLeft: 20 }}>
                                <Dropdown overlay={this.menu} trigger={['click']}>
                                    <Button>
                                        {this.state.typeName} <Icon type="down" />
                                    </Button>
                                </Dropdown>
                            </div>

                        </div>
                        <div style={{ wordSpacing: 2, textAlign: "left" }}>
                            <TextArea
                                rows={4}
                                placeholder="Enter detail of post"
                                onChange={this.onDetailChange}
                                value={this.state.detail}
                            />
                        </div>
                    </div>
                    <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", borderRadius: 50, justifyContent: "center", width: '80%' }}>
                        <div style={{ width: "90%", margin: 50 }}>
                            <Row>
                                <Col span={6}><div style={{ marginLeft: 50 }}><Avatar size={120} src={this.state.imageUrl} /> </div></Col>
                                <Col span={6}>
                                    <div style={{ flexDirection: 'row', display: 'flex' }}>
                                        <Title level={4}>ชื่อผู้ใช้ : </Title><label style={{ fontSize: 18 }}>&nbsp; &nbsp;{this.state.user.name}</label>
                                    </div>
                                    <div style={{ flexDirection: 'row', display: 'flex' }}>
                                        <Title level={4}>ความถนัด : </Title><label style={{ fontSize: 18 }}>&nbsp; &nbsp;{this.state.user.aptitude}</label>
                                    </div>
                                    <div style={{ flexDirection: 'row', display: 'flex' }}>
                                        <Title level={4}>สิ่งที่สนใจ : </Title><label style={{ fontSize: 18 }}>&nbsp; &nbsp;{this.state.user.interested}</label>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                        <div style={{ textAlign: "center", alignItems: "center" }}>
                            <div style={{ margin: 10, marginTop: 50, justifyContent: "left", display: "flex", paddingLeft: 100 }} >
                                <h2>ช่องทางการติดต่อ</h2>
                            </div>
                            <div style={{ justifyContent: "center", alignItems: "center", marginLeft: 100, marginBottom: 50 }}>
                                <Row>
                                    <Col span={6}>
                                        <img src={require('../assets/images/Facebook-icon.png')} height={50} />:
                                        <label style={{ marginTop: 5, marginLeft: 14, fontSize: 18 }}>{this.state.user.facebook}</label>
                                    </Col>
                                    <Col span={6}>
                                        <img src={require('../assets/images/line-icon.png')} height={50} />:
                                        <label style={{ marginTop: 5, marginLeft: 14, fontSize: 18 }}>{this.state.user.line}</label>
                                    </Col>
                                    <Col span={6} style={{ flexDirection: 'row', display: 'flex' }}>
                                        <div style={{ marginTop: 10 }}>
                                            <Icon type="phone" style={{ fontSize: 30 }} />:
                                    </div>
                                        <label style={{ marginTop: 5, marginLeft: 14, fontSize: 18 }}>{this.state.user.phone}</label>
                                    </Col>
                                </Row>
                            </div>
                            <Button
                                type="primary"
                                size="large"
                                style={{ margin: 20, backgroundColor: '#00cc66', borderColor: '#00cc66', borderRadius: 200 }}
                                onClick={() => this.onClickEditPost()}
                            >
                                แก้ไขโพส
                            </Button>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
