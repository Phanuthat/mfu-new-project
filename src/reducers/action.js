export const addUser = (user) => (
    {
        type: 'ADD_USER',
        user: user
    }
)

export const logOut = () => (
    {
        type: 'LOGOUT',
    }
)