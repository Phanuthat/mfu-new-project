import React, { Component } from 'react';
import { Carousel, Button } from 'antd';
class MenuBar extends Component {

    render() {
        
        return (
            <div>
                <div className="Header">
                    <div style={{ flex: 1 }}>
                        <h1 style={{ color: 'white', fontSize: 40 }}>MFU Exchange Knowledge</h1>
                    </div>
                    <div>
                        <Button size="large" style={{ borderRadius: 50, margin: 10}}>สมัครสมาชิก</Button>
                    </div>
                    <div>
                        <Button size="large" style={{ borderRadius: 50, margin: 10}}>เข้าสู่ระบบ</Button>
                    </div>
                </div>
            </div>
        )
    }

}

export default MenuBar;