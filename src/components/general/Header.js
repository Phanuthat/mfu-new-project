import React, { Component } from 'react';
import { Carousel, Button } from 'antd';
import { withRouter } from 'react-router'
class Header extends Component {

    onClickLoginButton = () => {
        this.props.history.push('/login');
    }
    onClickRegisterButton = () => {
        this.props.history.push('/register');
    }
    onClickLogo = () => {
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <div className="Header">
                    <div style={{ flex: 1 }}>
                        <a onClick={() => this.onClickLogo()}>
                            <h1 style={{ color: 'white', fontSize: 40 }}>MFU Exchange Knowledge</h1>
                        </a>
                    </div>
                    <div>
                        <Button
                            size="large"
                            style={{ borderRadius: 50, margin: 10 }}
                            onClick={() => this.onClickRegisterButton()}
                        >
                            สมัครสมาชิก
                            </Button>
                    </div>
                    <div>
                        <Button
                            size="large"
                            style={{ borderRadius: 50, margin: 10 }}
                            onClick={() => this.onClickLoginButton()}
                        >
                            เข้าสู่ระบบ
                            </Button>
                    </div>
                </div>
            </div>
        )
    }

}

export default withRouter(Header)
