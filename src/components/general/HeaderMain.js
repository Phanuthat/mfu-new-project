import React, { Component } from 'react';
import { Menu, Icon, Dropdown, Avatar } from 'antd';
import { withRouter } from 'react-router'

class HeaderMain extends Component {

    state = {
        imageUrl: '',
        email: '',
        user: []
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const email = jsonStr && JSON.parse(jsonStr).email;
        const user = jsonStr && JSON.parse(jsonStr)
        this.setState({ user: user })
        var imageUrl = jsonStr && JSON.parse(jsonStr).imageUrl;
        if (!imageUrl) {
            imageUrl = 'https://sv1.picz.in.th/images/2019/06/12/1tZYEN.png';
        }
        this.setState({ email, imageUrl });
        if (user.role === 'admin') {
            this.menu = (
                <Menu>
                    <Menu.Item onClick={() => this.onClickManagePost()}><Icon type="form" />Manage Post</Menu.Item>
                    <Menu.Item onClick={() => this.onClickManageKnowledge()}><Icon type="message" />Manage Knowledge</Menu.Item>
                    <Menu.Item onClick={() => this.onClickManageAdmin()}><Icon type="font-colors" />Manage Admin</Menu.Item>
                    <Menu.Item onClick={() => this.props.history.push('/chart')}><Icon type="area-chart" />Admin Dashboard</Menu.Item>
                    <Menu.Item onClick={() => this.onClickLogout()}><Icon type="logout" />Log Out</Menu.Item>
                </Menu>
            )
        } else {
            this.menu = (
                <Menu>
                    <Menu.Item onClick={() => this.onClickLogout()}><Icon type="logout" />Log Out</Menu.Item>
                </Menu>
            )
        }
    }

    onClickProfile = () => {
        this.props.history.push('/profile');
    }

    onClickManagePost = () => {
        this.props.history.push('/managepost');
    }

    onClickManageKnowledge = () => {
        this.props.history.push('/manageknowledge');
    }

    onClickManageAdmin = () => {
        this.props.history.push('/manageadmin');
    }

    onClickLogout = () => {
        localStorage.setItem(
            'user-data',
            JSON.stringify({
                isLoggedIn: false
            })
        );
        setTimeout(() => {
            this.props.history.push('/');
        }, 1500);
    }

    onClickLogo = () => {
        this.props.history.push('/');
    }

    onClickPost = () => {
        this.props.history.push('/post')
    }

    menu = (
        <div></div>
    )

    render() {
        return (
            <div>
                <div className="Header">
                    <div style={{ flex: 1 }}>
                        <a onClick={() => this.onClickLogo()}>
                            <h1 style={{ color: 'white', fontSize: 40 }}>MFU Exchange Knowledge</h1>
                        </a>
                    </div>
                    <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', marginRight: 40 }}>
                        <a onClick={() => this.onClickPost()}>
                            <h2>หน้าแรก</h2>
                        </a>
                    </div>
                    <Icon type="bell" theme="filled" style={{ fontSize: 30, marginTop: 10, marginRight: 20 }} />
                    <div style={{ marginRight: 20 }}>
                        <Dropdown overlay={this.menu} trigger={['click']}>
                            <div>
                                <a className="ant-dropdown-link" onClick={() => this.onClickProfile()}>
                                    <Avatar size={52} src={this.state.imageUrl} />&nbsp;&nbsp;<label style={{ color: 'white', fontSize: 18 }}>{this.state.email}</label>&nbsp;
                            </a>
                                <Icon type="caret-down" style={{ fontSize: 20 }} />
                            </div>
                        </Dropdown>
                    </div>
                </div>
            </div>
        )
    }

}

export default withRouter(HeaderMain)